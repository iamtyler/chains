/****************************************************************************
*
*   test/route.js
*   chains
*
***/

"use strict";

var assert = require("assert"),

    Router = require("../route").Router;

describe("Router", function () {
    describe("#constructor", function () {
        it("should create new Router", function () {
            var router = new Router();
            assert(router);
        });
    });

    describe("#register", function () {
        it("should register a simple route", function () {
            var router = new Router();
            router.get("/thing");

            assert.equal(router.routes.length, 1);
            var route = router.routes[0];
            assert.equal(route.method, "GET");
            assert.equal(route.path, "/thing");
            assert.equal(route.chain.length, 0);
        });
    });

    describe("#with", function () {
        it("should register with prefix", function () {
            var router = new Router();
            router.with("/prefix", (router) => {
                router.get("/thing");
            });

            assert.equal(router.routes.length, 1);
            var route = router.routes[0];
            assert.equal(route.method, "GET");
            assert.equal(route.path, "/prefix/thing");
            assert.equal(route.chain.length, 0);
        });

        it("should register with nested prefix", function () {
            var router = new Router();
            router.with("/prefix", (router) => {
                router.with("/nested", (router) => {
                    router.get("/thing");
                });
            });

            assert.equal(router.routes.length, 1);
            var route = router.routes[0];
            assert.equal(route.method, "GET");
            assert.equal(route.path, "/prefix/nested/thing");
            assert.equal(route.chain.length, 0);
        });

        it("should register with multiple prefixes", function () {
            var router = new Router();
            router.with("/some", function (router) {
                router.get("/thing");
            });
            router.with("/any", function (router) {
                router.get("/thing");
            });

            assert.equal(router.routes.length, 2);
            assert.equal(router.routes[0].path, "/some/thing");
            assert.equal(router.routes[1].path, "/any/thing");
        });
    });

    describe("#route", function () {
        var router = new Router(),
            result = {};
        router.get("/user/:id", (state) => {
            result.id = true;
        });
        router.get("/users", (state) => {
            result.list = true;
        });

        it("should get id from path param", function () {
            var handler = router.route("GET", "/user/USER_ID");
            result = {};
            assert.ok(handler);
            handler.execute();
            assert.equal(result.id, true);
        });

        it("should hit list route", function () {
            var handler = router.route("GET", "/users");
            result = {};
            assert.ok(handler);
            handler.execute();
            assert.equal(result.list, true);
        });
    });
});
