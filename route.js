/****************************************************************************
*
*   route.js
*   chains
*
***/


/****************************************************************************
*
*   RouteData
*
***/

class RouteData {

    //=======================================================================
    constructor (chain, params) {
        // Save data
        this._chain = chain;
        this._params = params;
    }

    //=======================================================================
    get chain() { return this._chain; }
    get params() { return this._params; }
}


/****************************************************************************
*
*   Handler
*
***/

class Handler {

    //=======================================================================
    constructor (data, args) {
        // Duplicate chain so it can be modified
        this._chain = data.chain.slice();

        // Generate params that came from the path
        this._params = {};
        for (let i in data.params) {
            // Pull names from data.params and values from args
            this._params[data.params[i]] = args[i];
        }
    }

    //=======================================================================
    get params () { return this._params; }

    //=======================================================================
    execute (state) {
        // Ensure state is something
        state = state || {};

        // Duplicate chain so it can be modified
        let chain = this._chain.slice();

        // Closure function for iterating chain
        function next () {
            let link = chain.shift();
            if (link) {
                link(state, next);
            }
        };

        // Kick off chain
        next();
    }
}


/****************************************************************************
*
*   Node
*
***/

class Node {

    //=======================================================================
    constructor () {
        this._methods = {};
        this._values = {};
        this._variable = null;
    }

    //=======================================================================
    register (method, parts, chain, params) {
        // Allow missing params argument
        params = params || [];

        // Set handler if no parts left
        if (parts.length === 0) {
            return this._methods[method] = new RouteData(chain, params);
        }

        // Get the next part
        const part = parts.shift();
        let node = null;

        // Create node
        if (part.startsWith(":")) {
            // Get or create variable node
            node = this._variable || (this._variable = new Node());

            // Save param name
            params.push(part.substring(1));
        }
        else {
            // Get or create node for part by value
            node = this._values[part] || (this._values[part] = new Node());
        }

        // Continue registration
        return node.register(method, parts, chain, params);
    }

    //=======================================================================
    match (method, parts, args) {
        // Allow missing args
        args = args || [];

        // Find route data by method
        if (parts.length === 0) {
            const data = this._methods[method];
            return data && new Handler(data, args);
        }

        // Get a part and try to look up a corresponding node
        const part = parts.shift();
        let node = this._values[part];

        // No node found but variable specified
        if (!node && this._variable) {
            node = this._variable;

            // Save argument
            args.push(part);
        }

        // Return nothing or continue to match
        return node && node.match(method, parts, args);
    }
}


/****************************************************************************
*
*   Router
*
***/

class Router {

    //=======================================================================
    constructor () {
        // The beginning of the routing tree
        this._root = new Node();

        // Info about all registered routes, available for debug display
        this._routes = [];

        // Tracking data for registering with prefix
        this._prefix = {
            stack : [],
            parts : []
        };
    }

    //=======================================================================
    get routes() { return this._routes; }

    //=======================================================================
    with (path, callback) {
        // Get prefix object
        const prefix = this._prefix;

        // Stash current parts
        prefix.stack.push(prefix.parts);

        // Duplicate and extend current parts
        prefix.parts = prefix.parts.concat(splitPath(path));

        // Invoke callback
        callback(this);

        // Revert to previous parts
        prefix.parts = prefix.stack.pop();
    }

    //=======================================================================
    register (method, path) {
        var parts = this._prefix.parts.concat(splitPath(path)),
            chain = flattenChain(Array.prototype.slice.call(arguments, 2));

        // Rebuild full path
        path = "/" + parts.join("/");

        // Register route
        this._root.register(method, parts, chain);

        // Record route data
        this._routes.push({
            method : method,
            path   : path,
            chain  : chain
        });
    }

    //=======================================================================
    get (path) {
        this.register("GET", path, Array.prototype.slice.call(arguments, 1));
    }

    //=======================================================================
    put (path) {
        this.register("PUT", path, Array.prototype.slice.call(arguments, 1));
    }

    //=======================================================================
    post (path) {
        this.register("POST", path, Array.prototype.slice.call(arguments, 1));
    }

    //=======================================================================
    delete (path) {
        this.register("DELETE", path, Array.prototype.slice.call(arguments, 1));
    }

    //=======================================================================
    route (method, path) {
        return this._root.match(method, splitPath(path));
    }
}


/****************************************************************************
*
*   Local functions
*
***/

//===========================================================================
function splitPath (path) {
    // Support empty path
    if (path === "/") {
        return [];
    }

    // Split path and remove empty parts
    return path.split("/").filter((part) => part.length > 0);
}

//===========================================================================
function flattenChain (chain) {
    // Create array from non-array value
    if (!Array.isArray(chain)) {
        return [ chain ];
    }

    // Reduce array
    return chain.reduce((result, current) => {
        return result.concat(flattenChain(current));
    }, []);
}


/****************************************************************************
*
*   Exports
*
***/

exports.Router = Router;
